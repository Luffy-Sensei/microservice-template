package com.tenor.sf.jobflow.microservicename;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class MicroservicenameApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroservicenameApplication.class, args);
	}
}
